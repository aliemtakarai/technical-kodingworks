<?php

return [
    'role_structure' => [
        'administrator' => [
            'users' => 'c,r,u,d',
            'hotels' => 'c,r,u,d',
            'rooms' => 'c,r,u,d',
            'roomOrders' => 'c,r,u,d',
            'hotelManagers' => 'c,r,u,d',
            'hotelStaff' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'manager' => [
            'hotels' => 'r,u',
            'rooms' => 'r,u',
            'roomOrders' => 'r,u,d',
            'hotelManagers' => 'c,r,u,d',
            'hotelStaff' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'staff' => [
            'hotels' => 'r',
            'rooms' => 'r',
            'roomOrders' => 'r',
            'hotelStaff' => 'r',
            'profile' => 'r,u'
        ],
        'member' => [
            'hotels' => 'r',
            'rooms' => 'r',
            'roomOrders' => 'c,r,u',
            'profile' => 'r,u'
        ]
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
