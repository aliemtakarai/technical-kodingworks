<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','role:administrator|manager|staff']], function () {
    Route::view('/my-account', 'myAccount')->name('account');
    Route::group(['prefix' => 'hotels'], function () {
        Route::group(['middleware' => ['permission:read-hotels']], function () {
            Route::get('/', 'HotelController@index')->name('hotel.index');
            Route::get('/get-data', 'HotelController@getData')->name('hotel.getData');
        });
        Route::group(['middleware' => ['permission:create-hotels']], function () {
            Route::get('/create', 'HotelController@create')->name('hotel.create');
            Route::post('/store', 'HotelController@store')->name('hotel.store');
        });
        Route::group(['middleware' => ['permission:update-hotels']], function () {
            Route::get('/{id}/edit', 'HotelController@edit')->name('hotel.edit');
            Route::post('/{id}/update', 'HotelController@update')->name('hotel.update');
        });
        Route::group(['middleware' => ['permission:delete-hotels']], function () {
            Route::post('/{id}/delete', 'HotelController@destroy')->name('hotel.delete');
        });
        Route::get('/{id}/detail', 'HotelController@detail')->name('hotel.detail');

    });
    Route::group(['prefix' => 'users', 'middleware'=>['role:administrator']], function () {
        Route::get('/', 'UserController@index')->name('user.index');
        Route::get('/get-data', 'UserController@getData')->name('user.getData');
        Route::get('/create', 'UserController@create')->name('user.create');
        Route::post('/store', 'UserController@store')->name('user.store');
        Route::get('/{id}/edit', 'UserController@edit')->name('user.edit');
        Route::post('/{id}/update', 'UserController@update')->name('user.update');
        Route::post('/{id}/delete', 'UserController@destroy')->name('user.delete');
    });
    Route::get('/audit-logs', 'AuditLogController@index')->name('audit.index');
});
