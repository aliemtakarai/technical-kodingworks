<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::post('/auth/login-email', 'UserController@login');
    Route::post('/auth/logout', 'UserController@logout');

    Route::group(['middleware' => ['jwt.verify']], function () {
        Route::group(['middleware' => ['role:administrator']], function () {
            Route::get('/users', 'UserController@showAll');
            Route::post('/users/add', 'UserController@storeApi');
            Route::put('/users/{id}/update', 'UserController@updateApi');
            Route::delete('/users/{id}/delete', 'UserController@destroyApi');
        });

        Route::get('users/profile', 'UserController@profileApi');
        Route::put('users/profile/update', 'UserController@updateProfile');
        //hotels
        Route::get('/hotels', 'HotelController@showAll');
        Route::get('/hotels/all', 'HotelController@show');
        Route::get('/hotels/{id}/room', 'HotelController@room');
        Route::post('/hotels/add', 'HotelController@storeApi');
        Route::put('/hotels/{id}/update', 'HotelController@updateApi');
        Route::delete('/hotels/{id}/delete', 'HotelController@destroyApi');
        //order
        Route::post('/order/store', 'OrderController@storeOrder');
        Route::get('/order/detail', 'OrderController@detailOrder');
        Route::get('/order/get-order', 'OrderController@getOrder');
    });
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
