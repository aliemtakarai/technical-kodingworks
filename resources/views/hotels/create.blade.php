@extends('layouts.dashboard')

@section('title', 'Add Hotel')

@section('content')
    @card
        @slot('cardWidth')
            col-md-12
        @endslot
        @slot('content')
            <form action="{{ route('hotel.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Name*</label>
                    <input type="text" class="form-control" name="name" placeholder="Input Hotel Name">
                    @error('name')
                    <label style="color:red;">{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Image*</label>
                    <input type="file" class="form-control" name="image" id="exampleInputFile">
                    @error('image')
                    <label style="color:red;">{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Description*</label>
                    <textarea name="description" class="form-control"></textarea>
                    @error('description')
                    <label style="color:red;">{{ $message }}</label>
                    @enderror
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Room Type</td>
                                <td>Total Room</td>
                                <td>price</td>
                                <td>action</td>
                            </tr>
                        </thead>
                        <tbody id="row">
                            <tr>
                                <td><input type="text" name="roomType[]" class="form-control"></td>
                                <td><input type="number" name="total[]" class="form-control"></td>
                                <td><input type="number" name="price[]" class="form-control"></td>
                                <td><button type="button" class="btn btn-primary" onclick="addRow()"><i class="fa fa-plus"></i></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </form>
        @endslot
    @endcard
@endsection
@push('script')
    <script>
        var i = 1;

        function addRow(){
            i++
            $('#row').append('<tr id="row'+i+'">'+
                                    '<td><input type="text" name="roomType[]" class="form-control"></td>'+
                                    '<td><input type="number" name="total[]" class="form-control"></td>'+
                                    '<td><input type="number" name="price[]" class="form-control"></td>'+
                                    '<td><button type="button" class="btn btn-danger" onclick="deleteRow('+i+')"><i class="fa fa-trash-alt"></i></button></td>'+
                                '</tr>')
        }
        function deleteRow(i){
            $('#row'+i).remove()
        }
    </script>
@endpush
