@extends('layouts.dashboard')

@section('title', 'Data Hotel')

@section('content')
    @card
        @slot('cardWidth')
            col-md-12
        @endslot
        @slot('content')
            @permission('create-hotels')
            <div class="row">
                <a href="{{ route('hotel.create') }}" class="btn btn-primary">Add Data</a>
            </div>
            @endpermission
            <div class="row table-responsive">
                <table class="table" id="datatable">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Description</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        @endslot
    @endcard
    @modalDelete
    @endmodalDelete
    @toast
    @endtoast
@endsection
@push('style')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endpush
@push('script')
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $('#datatable').dataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ route('hotel.getData') }}',
      columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'name', name: 'name' },
          { data: 'description', name: 'description' },
          { data: 'action', name: 'action', orderable: false, searchable: false }
      ]
    });

    $('#modal-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var modal = $(this)
        $('form').attr('action', "/hotels/"+id+"/delete");
    });
    </script>
@endpush
