@extends('layouts.dashboard')

@section('title', 'Detail Hotel')

@section('content')
    @card
        @slot('cardWidth')
            col-md-12
        @endslot
        @slot('content')
            <form action="{{ route('hotel.update', $hotel->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Name*</label>
                    <input type="text" class="form-control" name="name" value="{{ $hotel->name }}" placeholder="Input Hotel Name" readonly>
                    @error('name')
                    <label style="color:red;">{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Image </label>
                    <br>
                    <img src="{{ Storage::url($hotel->image) }}" width="200" height="200">
                    @error('image')
                    <label style="color:red;">{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Description*</label>
                    <textarea name="description" class="form-control" readonly>{{ $hotel->description }}</textarea>
                    @error('description')
                    <label style="color:red;">{{ $message }}</label>
                    @enderror
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Room Type</td>
                                <td>Total Room</td>
                                <td>price</td>
                            </tr>
                        </thead>
                        <tbody id="row">
                            @isset($hotel->rooms)
                                @foreach ($hotel->rooms as $room)
                                    <tr id="row{{ $room->id }}">
                                        <td><input type="text" name="roomType[]" value="{{ $room->name }}" class="form-control" readonly></td>
                                        <td><input type="number" name="total[]" class="form-control" value="{{ $room->total }}" readonly></td>
                                        <td><input type="number" name="price[]" class="form-control" value="{{ $room->price }}" readonly></td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </form>
        @endslot
    @endcard
@endsection
