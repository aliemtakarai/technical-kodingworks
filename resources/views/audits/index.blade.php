@extends('layouts.dashboard')

@section('title', 'Audit Logs')

@section('content')
    @card
        @slot('cardWidth')
            col-md-12
        @endslot
        @slot('content')
            @permission('create-hotels')
            <div class="row">
                <a href="{{ route('hotel.create') }}" class="btn btn-primary">Add Data</a>
            </div>
            @endpermission
            <div class="row table-responsive">
                <table class="table" id="datatable">
                    <thead>
                        <tr>
                            <td>Method</td>
                            <td>URL</td>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach ($audits as $audit)
                                <tr>
                                    <td>{{ $audit->content }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </tbody>
                    <tbody>

                    </tbody>
                </table>
            </div>
        @endslot
    @endcard
    @modalDelete
    @endmodalDelete
    @toast
    @endtoast
@endsection
