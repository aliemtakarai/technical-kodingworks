@extends('layouts.app')

@section('title', 'My Account')

@section('content')
<div class="content">
    <div class="title m-b-sm lefts">
        My Account
    </div>
    <table border="1">
        <tr>
            <td>Name</td>
            <td>{{ Auth::user()->name }}</td>
        </tr>
        <tr>
            <td>Address</td>
            <td></td>
        </tr>
    </table>
</div>
@endsection
