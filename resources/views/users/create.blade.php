@extends('layouts.dashboard')

@section('title', 'Create User')

@section('content')
    @card
        @slot('cardWidth')
            col-md-12
        @endslot
        @slot('content')
        <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Name*</label>
                <input type="text" class="form-control" name="name" placeholder="Input User Name">
                @error('name')
                <label style="color:red;">{{ $message }}</label>
                @enderror
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" name="address" placeholder="Input Addresss">
                @error('name')
                <label style="color:red;">{{ $message }}</label>
                @enderror
            </div>
            <div class="form-group">
                <label>Email*</label>
                <input type="email" class="form-control" name="email" placeholder="Input Email Address">
                @error('name')
                <label style="color:red;">{{ $message }}</label>
                @enderror
            </div>
            <div class="form-group">
                <label>Password*</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Input User Name">
                <div class="form-check">
                    <input type="checkbox" onclick="show()" class="form-check-input">Show Password
                </div>
                @error('name')
                <label style="color:red;">{{ $message }}</label>
                @enderror
            </div>
            <div class="form-group">
                <label>Role</label>
                <select name="role" class="form-control" onchange="hotelShow()" id="role">
                    <option value="" hidden>Roles Option</option>
                    @foreach ($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="hotelOption">
                <label>Hotel</label>
                <select name="hotel" class="form-control">
                    <option value="" hidden>Hotel Option</option>
                    @foreach ($hotels as $hotel)
                        <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Save">
            </div>
        </form>
        @endslot
    @endcard
@endsection

@push('script')
<script>
    function show() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    $('#hotelOption').hide()
    function hotelShow() {
        let hotel = document.getElementById('role').value
        hotel == 2 || hotel == 3 ? $('#hotelOption').show() : $('#hotelOption').hide()
    }
</script>
@endpush
