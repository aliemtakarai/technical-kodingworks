<div class="container">
    <div class="row justify-content-center">
        <div class="{{ $cardWidth }}">
            <div class="card">
                <div class="card-header">@yield('title')</div>

                <div class="card-body">
                    {{ $content }}
                </div>
            </div>
        </div>
    </div>
</div>
