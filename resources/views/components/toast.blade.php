@push('style')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endpush
@push('script')
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    @if($message = Session::get('success'))
    <script>
    toastr.success("Success")
    </script>
    @endif
@endpush
