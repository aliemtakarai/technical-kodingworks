<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::insert(
            [
                'id'=>1,
                'name'=>'aliemtakarai',
                'email'=>'aliemtakarai@gmail.com',
                'password'=>bcrypt('password'),
                'address'=>'macanda'
            ],
            [
                'id'=>2,
                'name'=>'indra',
                'email'=>'indra@gmail.com',
                'password'=>bcrypt('password'),
                'address'=>'badak'
            ]
        );
    }
}
