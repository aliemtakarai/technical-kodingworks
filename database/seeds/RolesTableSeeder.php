<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "Administrator";
        $admin->save();

        $manager = new Role();
        $manager->name = "manager";
        $manager->display_name = "Hotel Manager";
        $manager->save();

        $staff = new Role();
        $staff->name = "staff";
        $staff->display_name = "Hotel Staff";
        $staff->save();

        $member = new Role();
        $member->name = "member";
        $member->display_name = "Hotel Member";
        $member->save();

        $user1 = User::find(1); //superadmin
        $user1->attachRole($admin);

        $user2 = User::find(2); //superadmin
        $user2->attachRole($manager);

        $create = new Permission();
        $create->name = 'create';
        $create->display_name = 'Create';
        $create->description = 'Access for Create';
        $create->save();

        $read = new Permission();
        $read->name = 'read';
        $read->display_name = 'Read';
        $read->description = 'Access for Read';
        $read->save();

        $update = new Permission();
        $update->name = 'edit';
        $update->display_name = 'Edit';
        $update->description = 'Access for Edit';
        $update->save();

        $delete = new Permission();
        $delete->name = 'delete';
        $delete->display_name = 'Delete';
        $delete->description = 'Access for Delete';
        $delete->save();

        $user1->attachPermissions([$create,$read,$update,$delete]);


    }
}
