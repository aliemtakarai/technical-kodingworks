<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('checkin_date');
            $table->date('checkout_date');
            $table->bigInteger('room_id')->unsigned();
            $table->enum('status',['booked','cancel']);
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('room_id')->references('id')->on('room_hotels');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
