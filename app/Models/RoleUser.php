<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table='role_user';

    public function roleUser()
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }
}
