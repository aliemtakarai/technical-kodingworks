<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomHotel extends Model
{
    //
    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel', 'hotel_id', 'id');
    }
}
