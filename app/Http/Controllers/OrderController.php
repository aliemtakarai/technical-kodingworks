<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use Validator;

class OrderController extends Controller
{
    public function storeOrder(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'checkin' => 'required|date',
            'checkout' => 'required|date',
            'room' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'Missing input fields'], 500);
        }

        try {
            $order = new Order;
            $order->checkin_date = date('Y-m-d', strtotime($request->checkin));
            $order->checkout_date = date('Y-m-d', strtotime($request->checkout));
            $order->room_id = $request->room;
            $order->status = 'booked';
            $order->user_id = $request->user()->id;
            $order->save();

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()], 500);
        }
    }

    public function detailOrder()
    {
        $order = Order::with('room.hotel','user')
                ->first();
        if (!$order) {
            return response()->json(['message'=>'data not found'], 404);
        } else {
            return response()->json(['data'=>$order]);
        }
    }

    public function getOrder()
    {
        $order = Order::with('room.hotel')
                ->paginate(10);

        return response()->json($order);
    }

    public function updateOrder(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'checkin' => 'required|date',
            'checkout' => 'required|date',
            'room' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'Missing input fields'], 500);
        }

        try {
            $order = Order::find($id);
            $order->checkin_date = date('Y-m-d', strtotime($request->checkin));
            $order->checkout_date = date('Y-m-d', strtotime($request->checkout));
            $order->room_id = $request->room;
            $order->status = 'booked';
            $order->user_id = $request->user()->id;
            $order->save();

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()], 500);
        }
    }
}
