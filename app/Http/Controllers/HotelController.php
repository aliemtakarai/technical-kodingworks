<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\RoomHotel;
use Validator, Storage, DataTables;

class HotelController extends Controller
{
    public function index()
    {
        return view('hotels.index');
    }

    public function getData()
    {
        if (\Auth::user()->hotel->hotel_id != "none") {
            $hotel = Hotel::where('id',\Auth::user()->hotel->hotel_id)->get();
        } else {
            $hotel = Hotel::all();
        }
        return DataTables::of($hotel)
            ->addIndexColumn()
            ->addColumn('action', function ($hotel){
                return '<a href="'.route('hotel.detail', $hotel->id).'" class="btn-success btn">Detail</a>
                                    <a href="'.route('hotel.edit', $hotel->id).'" class="btn-info btn" title="Edit Data">Edit</a>
                                    <a href="#" data-toggle="modal" data-target="#modal-delete" data-id="'.$hotel->id.'" class="btn-danger btn">Delete</a>
                                    ';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('hotels.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'image'=>'required|mimes:jpg,jpeg,png,bmp,tiff |max:4096',
            'description'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

            $hotel = new Hotel;
            $hotel->name = ucwords($request->name);
            $hotel->image = $request->file('image')->store('public/images');
            $hotel->description = $request->description;
            $hotel->save();

            $count = count($request->roomType);

            for ($i=0; $i < $count; $i++) {
                $data[] = [
                    'hotel_id' => $hotel->id,
                    'name' => ucwords($request->roomType[$i]),
                    'total' => $request->total[$i],
                    'price' => $request->price[$i]
                ];
            }

            RoomHotel::insert($data);

            return redirect()->route('hotel.index')->with(['success'=>'deleted success']);
    }

    public function edit($id)
    {
        $hotel = Hotel::find($id)
                ->with(['rooms'])->first();
        return view('hotels.edit', compact('hotel'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'description'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

            $hotel = Hotel::findOrFail($id);
            $hotel->name = ucwords($request->name);
            if ($request->file('image')) {
                $hotel->image = $request->file('image')->store('public/images');
            }
            $hotel->description = $request->description;
            $hotel->save();

            $count = count($request->roomType);

            $room = RoomHotel::where('hotel_id', $hotel->id);
            $room->delete();

            for ($i=0; $i < $count; $i++) {
                $data[] = [
                    'hotel_id' => $hotel->id,
                    'name' => ucwords($request->roomType[$i]),
                    'total' => $request->total[$i],
                    'price' => $request->price[$i]
                ];
            }

            RoomHotel::insert($data);

            return redirect()->route('hotel.index')->with(['success'=>'updated success']);
    }

    public function detail($id)
    {
        $hotel = Hotel::find($id)
                ->with(['rooms'])->first();
        return view('hotels.detail', compact('hotel'));
    }

    public function destroy($id)
    {
        $hotel = Hotel::findOrFail($id);
        Storage::delete($hotel->image);
        $hotel->delete();

        return redirect()->route('hotel.index')->with(['success'=>'deleted success']);
    }

    #rest api
    public function showAll(Request $request)
    {
        $hotel = Hotel::paginate(10);
        return response()->json(
            [
                'message'=>'success',
                'data'=>$hotel
            ]
        );
    }

    public function show(Request $request)
    {
        $hotel = Hotel::all();
        return response()->json(
            [
                'message'=>'success',
                'data'=>$hotel
            ]
        );
    }

    public function storeApi(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'image'=>'required|mimes:jpg,jpeg,png,bmp,tiff |max:4096',
            'description'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'missing field input'], 500);
        }

        try {
            $hotel = new Hotel;
            $hotel->name = ucwords($request->name);
            $hotel->image = $request->file('image')->store('public/images');
            $hotel->description = $request->description;
            $hotel->save();

            $count = count($request->roomType);

            for ($i=0; $i < $count; $i++) {
                $data[] = [
                    'hotel_id' => $hotel->id,
                    'name' => ucwords($request->roomType[$i]),
                    'total' => $request->total[$i],
                    'price' => $request->price[$i]
                ];
            }

            RoomHotel::insert($data);

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()], 500);
        }
    }

    public function updateApi(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'description'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'missing field input'], 500);
        }

        try {
            $hotel = Hotel::findOrFail($id);
            $hotel->name = ucwords($request->name);
            if ($request->file('image')) {
                $hotel->image = $request->file('image')->store('public/images');
            }
            $hotel->description = $request->description;
            $hotel->save();

            $count = count($request->roomType);

            $room = RoomHotel::where('hotel_id', $hotel->id);
            $room->delete();

            for ($i=0; $i < $count; $i++) {
                $data[] = [
                    'hotel_id' => $hotel->id,
                    'name' => ucwords($request->roomType[$i]),
                    'total' => $request->total[$i],
                    'price' => $request->price[$i]
                ];
            }

            RoomHotel::insert($data);

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }

    public function destroyApi($id)
    {
        try {
            $hotel = Hotel::findOrFail($id);
            Storage::delete($hotel->image);
            $hotel->delete();

            return response()->json(['message'=>'success'],200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }

    public function room($id)
    {
        try {
            $hotel = Hotel::find($id)
                ->with(['rooms'])->first();
            return response()->json(
                [
                    'message'=>'success',
                    'data'=>$hotel
                ]
            );
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }
}
