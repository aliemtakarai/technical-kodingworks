<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AuditLog;

class AuditLogController extends Controller
{
    public function index()
    {
        $audits = AuditLog::select('content')
                ->where('type', 'request')
                ->paginate(10);
        // return view('audits.index', compact('audits'));
        return $audits;
    }
}
