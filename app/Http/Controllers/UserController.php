<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Hotel;
use App\Models\User;
use App\Models\HotelUser;
use App\Models\RoleUser;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DataTables, JWTAuth;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    public function create()
    {
        $roles = Role::all();
        $hotels = Hotel::all();
        return view('users.create', compact('roles','hotels'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user = new User;
        $user->name = $request->name;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->attachRole($request->role);

        if (isset($request->hotel)) {
            $hotel = new HotelUser;
            $hotel->user_id = $user->id;
            $hotel->hotel_id = $request->hotel;
            $hotel->save();
        }

        return redirect()->route('user.index')->with(['success'=>'User Created Success']);
    }

    public function getData()
    {
        $user = User::with('role.roleUser')->get();

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action', function ($user){
                return '<a href="'.route('user.edit', $user->id).'" class="btn-info btn" title="Edit Data">Edit</a>
                                    <a href="#" data-toggle="modal" data-target="#modal-delete" data-id="'.$user->id.'" class="btn-danger btn">Delete</a>
                                    ';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function edit($id)
    {
        $user = User::with(['role','hotel'])
                ->where('id', $id)
                ->first();
        $roles = Role::all();
        $hotels = Hotel::all();
        return view('users.edit', compact('user','roles','hotels'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user = User::find($id);
        $user->name = $request->name;
        $user->address = $request->address;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        $role = RoleUser::where('user_id', $user->id)->first();
        $user->detachRole($role->role_id);
        $user->attachRole($request->role);

        if (isset($request->hotel)) {
            HotelUser::where('user_id', $id)
            ->update(
                [
                    'user_id'=>$id,
                    'hotel_id'=>$request->hotel
                ]
            );
        }

        return redirect()->route('user.index')->with(['success'=>'User Updated Success']);
    }

    public function destroy($id)
    {
        $hotel = Hotel::findOrFail($id);
        $hotel->delete();

        return redirect()->route('user.index')->with(['success'=>'User Delete Success']);
    }

    //Rest api

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>'missing fields attribute input'], 500);
        }

        $credentials = $request->only(['email','password']);
        $token = JWTAuth::attempt($credentials);
        try {
            if (!$token) {
                return response()->json(['message' => 'invalid_credentials'], 400);
            } else {
                return response()->json(
                    [
                        'message'=>'login success',
                        'access_token'=>$token,
                        'token_type'=>'bearer'
                    ], 200
                );
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token'], 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate(JWTAuth::parseToken()->authenticate());
            return response()->json(
                [
                    'message'=>'logout success'
                ], 200
            );
        } catch (JWTException $e) {
            return response()->json(['message'=>$e->getMessage()], 500);
        }
    }

    public function showAll()
    {
        try {
            $user = User::with('role.roleUser')->paginate(10);
            return response()->json(
                [
                    'message'=>'success',
                    'data'=>$user
                ]
            );
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }

    public function storeApi(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>'missing field input'], 500);
        }
        try {
            $user = new User;
            $user->name = $request->name;
            $user->address = $request->address;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $user->attachRole($request->role);

            if (isset($request->hotel)) {
                $hotel = new HotelUser;
                $hotel->user_id = $user->id;
                $hotel->hotel_id = $request->hotel;
                $hotel->save();
            }

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()], 500);
        }
    }

    public function updateApi(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'missing field input'], 500);
        }

        try {
            $user = User::find($id);
            $user->name = $request->name;
            $user->address = $request->address;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            $role = RoleUser::where('user_id', $user->id)->first();
            $user->detachRole($role->role_id);
            $user->attachRole($request->role);

            if (isset($request->hotel)) {
                HotelUser::where('user_id', $id)
                ->update(
                    [
                        'user_id'=>$id,
                        'hotel_id'=>$request->hotel
                    ]
                );
            }

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }

    public function destroyApi($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }

    public function profileApi(Request $request)
    {
        try {
            $data = $request->user();
            return response()->json(['data'=>$data], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage()]);
        }
    }
     public function updateProfile(Request $request)
     {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'missing field input'], 500);
        }
        try {
            $user = findOrFail($request->user()->id);
            $user->name = $request->name;
            $user->address = $request->address;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            return response()->json(['message'=>'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['message'=>$th->getMessage]);
        }
     }

}
